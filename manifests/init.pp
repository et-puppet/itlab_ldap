# Class: itlab_ldap
# ===========================
#
# Build a master or replica LDAP server
#
# Examples
# --------
#
# @example
#    class { 'itlab_ldap': }
#
# Authors
# -------
#
# Ian Douglas <idouglas@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# Copyright
# ---------
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class itlab_ldap (
  $ldap_dirs,
  $volume_files
) {

  include itlab_ldap::packages

  file {
    [
      $ldap_dirs
    ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/start':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
    source => "puppet:///modules/${module_name}/start_ldap.sh"
  }

  file { '/etc/ldap/slapd.d/cn=config':
    ensure  => directory,
    purge   => true,
    recurse => true,
    force   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0750'
  }

  # empty files that need to exist so that they can be replaced with volumes
  file {
    [
      $volume_files
    ]:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    content => ''
  }

  file {
    [
      '/var/lib/ldap/data.mdb',
      '/var/lib/ldap/lock.mdb'
    ]:
    ensure  => absent
  }

}
