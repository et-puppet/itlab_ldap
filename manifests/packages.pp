# == Class: itlab_ldap::packages
#
# Installs / updates all the LDAP packages
#
# Included by itlab_ldap module
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
# Ian Douglas <idouglas@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class itlab_ldap::packages (
  $install,
  $uninstall,
  $package_type,
){

  Package { provider => $package_type }

  include stdlib

  $only_install = difference($install, $uninstall)

  $installing = join($only_install, ' ')
  $uninstalling = join($uninstall, ' ')

  ensure_packages($only_install, { ensure => present })
  ensure_packages($uninstall, { ensure => absent })

  if ($::osfamily == 'Debian') {
    exec { 'apt-mark':
      command => "/usr/bin/apt-mark manual ${installing}",
      require => Package[$only_install],
    }
  }

}
