#!/bin/bash

DUMP_DIR=${DUMP_DIR:-/var/backups/ldap}
DATA_DIR=${DATA_DIR:-/var/lib/ldap}
CONF_DIR=${CONF_DIR:-/etc/ldap/slapd.d}
CONF_SRC=${CONF_SRC:-/etc/ldap/slapd.src}
LOG_LEVEL=${LOG_LEVEL:-16383}
LDAP_BASE=${LDAP_BASE:-dc=itlab,dc=stanford,dc=edu}
DUMP_HISTORY=${DUMP_HISTORY:-4}
KRB5_KTNAME=${KRB5_KTNAME:-/etc/ldap.keytab}
KRB5_CCDIR=${KRB5_CCDIR:-/var/tmp/ccache}
KRB5CCNAME=${KRB5CCNAME:-FILE:/var/tmp/ccache/krb5cc_0}
REALM=${REALM:-itlab.stanford.edu}

ACTION=$1

function restore_config () {
  local config=$1
  local force=$2
  local new_hash=$(sha256sum $config)

  if [ -z "${force}" ]; then
    # assume we need to update the config
    force=UPDATE

    # checksum of last-loaded config
    if [ -f ${CONF_DIR}/config.hash ]; then
      old_hash=$(cat ${CONF_DIR}/config.hash)
      echo -e "comparing configuration checksums"
      echo -e "${old_hash% *}: last-loaded configuration"
      echo -e "${new_hash% *}: configuration in ${config}"
      if [ ${new_hash% *} == ${old_hash% *} ]; then
	# unset force - no update needed
	force=
	echo "Current configuration is most recent available."
      fi
    fi
  fi    

  # restore config if checksums differ, or if no checksum file exists for last-loaded config
  if [ -n "${force}" ]; then
    rm -rf ${CONF_DIR}/*
    stat --format "Loading config from ldif '%n' modified on %y having hash ${new_hash% *}" ${config}
    slapadd -v -F ${CONF_DIR} -b cn=config -l ${config} || {
      echo "Config data load did not succeed. Exiting." >&2
      # remove restored data since config faulty, forcing reload on next boot
      rm -rfv ${DATA_DIR}/*.mdb
      exit 1
    }
    # write checksum to compare against source on next boot
    sha256sum ${config} >${CONF_DIR}/config.hash
  fi

}

function mdb_load() {
  
  echo -e "Assessing data state for ldap role: ${LDAP_ROLE}"

  config=${CONF_SRC}/${LDAP_ROLE}.ldif
  # if no configuration file found in source directory, exit
  if [[ ! -f ${config} ]]; then
    echo "No configuration file found in ${CONF_SRC}/${LDAP_ROLE}.ldif. Exiting." >&2
    exit 1
  fi

  file_data=${DUMP_DIR}/data.mdb.0
  rebuild_data=${DUMP_DIR}/rebuild/data.ldif
  rebuild_config=${DUMP_DIR}/rebuild/config.ldif

  # if this file exists, force rebuild from ldif
  rebuild_force=${DUMP_DIR}/rebuild/force-rebuild

  # if no current snapshot, and either no data ldif or no config ldif in rebuild directory, exit
  if [[ ! -f "$file_data"  && ( ! -f "$rebuild_data" || ! -f "$rebuild_config" ) ]] ; then
    echo "No mdb snapshot found in ${DUMP_DIR} and no config/data ldifs to load. Exiting." >&2
    exit 1
  fi

  # if either no current snapshot, or rebuild file is present, set REBUILD flag
  if [[ -f "$file_data" && ! -f "$rebuild_force" ]]; then

    # NO REBUILD
    # most recent data.mdb in backup

    # restore data and config if newer backup exists
    if [ ${file_data} -nt ${DATA_DIR}/data.mdb ]; then
      echo -e "Backup data is newer than current mdb.\nLoading most recent data and config..."
      find ${DATA_DIR} -type f -name '*.mdb' -delete
      stat --format "restoring snapshot '%n' modified on %y" ${file_data}
      cp -v ${file_data} ${DATA_DIR}/data.mdb
    else
      echo "Current mdb is most recent data available."
    fi
    restore_config ${config}

  else

    # REBUILD
    if [[ -f "$rebuild_force" ]]; then
      echo -e "Attempting forced rebuild."
    else
      echo -e "Attempting data and config load from ldif."
    fi

    if [[ ! -f "$rebuild_data"  || ! -f "$rebuild_config" ]]; then
      echo "The required data and config ldif files not found in ${DUMP_DIR}/rebuild. Exiting." >&2
      exit 1
    fi

    stat --format "Loading config from ldif '%n' modified on %y" ${rebuild_config}

    # purge stale data and config
    find ${DATA_DIR} -type f -name '*.mdb' -delete

    restore_config ${rebuild_config} REBUILD

    # load data after optionally unzipping
    stat --format "Found data file '%n' modified on %y" ${rebuild_data}
    echo "Loading data from '${rebuild_data}'"
    start=$SECONDS
    slapadd -F ${CONF_DIR} -b ${LDAP_BASE} -l ${rebuild_data} || {
      echo "Data load did not succeed (return code $?). Exiting." >&2
      exit 1
    }
    finish=$SECONDS
    runtime=$(echo "scale=2; $((finish-start)) / 60" | bc)
    echo "Data load completed in approximately $runtime minutes"
    rm -rf ${rebuild_force}
  fi

}

function mdb_dump() {

  [ -x /usr/bin/mdb_copy ] || {
    echo "mdb_copy not installed. Exiting." >&2
    exit 1
  }

  # clean up from any previous failures
  rm -f ${DUMP_DIR}/data.mdb
  rm -f ${DUMP_DIR}/config.ldif


  # mdb
  echo "Performing snapshot using mdbcopy"
  /usr/bin/mdb_copy ${DATA_DIR} ${DUMP_DIR} || {
    echo "Database dump did not succeed. Exiting." >&2
    rm -f ${DUMP_DIR}/data.mdb
    exit 1
  }

  # config
  echo "Performing config dump using slapcat"
  slapcat -F /etc/ldap/slapd.d -b cn=config > ${DUMP_DIR}/config.ldif || {
    echo "Config dump did not succeed. Exiting." >&2
    rm -f ${DUMP_DIR}/config.ldif
    exit 1
  }

  # dump succeeded, so roll over the old dumps
  local new=${DUMP_HISTORY}

  while [ ${new} -gt 0 ]; do
    old=$((new-1))

    rm -f ${DUMP_DIR}/data.mdb.${new}
    [ -f  ${DUMP_DIR}/data.mdb.${old} ] && \
      mv ${DUMP_DIR}/data.mdb.${old} ${DUMP_DIR}/data.mdb.${new}

    rm -f ${DUMP_DIR}/config.ldif.${new}
    [ -f  ${DUMP_DIR}/config.ldif.${old} ] && \
      mv ${DUMP_DIR}/config.ldif.${old} ${DUMP_DIR}/config.ldif.${new}

    new=${old}
  done

  mv ${DUMP_DIR}/data.mdb ${DUMP_DIR}/data.mdb.0
  mv ${DUMP_DIR}/config.ldif ${DUMP_DIR}/config.ldif.0

}

function ldif_dump() {
  echo -e "Performing ldif dump for ${LDAP_ROLE} database"
  
  # wait for slapd process to exit before backing up
  tries=40
  while pgrep -x slapd && [ $tries -gt 0 ]; do
    sleep 3
    tries=$((--tries))
  done

  pgrep -x slapd && {
    echo "slapd shutdown is hung so backup cannot continue. Exiting." >&2
    exit 1
  }

  # dump
  slapcat -v -b ${LDAP_BASE} > ${DUMP_DIR}/data.ldif || {
    echo "Database dump did not succeed. Exiting." >&2
    rm -f ${DUMP_DIR}/data.ldif
    exit 1
  }

  # dump succeeded, so roll over the old dumps
  local new=${DUMP_HISTORY}

  while [ ${new} -gt 0 ]; do
    old=$((new-1))

    rm -f ${DUMP_DIR}/data.ldif.${new}
    [ -f  ${DUMP_DIR}/data.ldif.${old} ] && \
      mv ${DUMP_DIR}/data.ldif.${old} ${DUMP_DIR}/data.ldif.${new}

    new=${old}
  done

  mv ${DUMP_DIR}/data.ldif ${DUMP_DIR}/data.ldif.0

}

function kinit_svc() {

  ccfile=${KRB5CCNAME#*:}
  kinit -k -t ${KRB5_KTNAME} -c ${ccfile} ldap/${HOSTNAME}
  klist -v

}

case "${ACTION}" in
  preflight)
    mdb_load && kinit_svc
    ;;

  dump)
    mdb_dump
    ;;

  ldifdump)
    ldif_dump
    ;;

  krenew)
    kinit_svc
    ;;

  ldap)
#    /usr/sbin/slapd -h "ldap:/// ldaps:/// ldapi:///" -F /etc/ldap/slapd.d -d ${LDAP_LOG_LEVEL}
    /usr/sbin/slapd -h "ldap:/// ldaps:/// ldapi:///" -F /etc/ldap/slapd.d -d 255
    ;;

  *)
    echo "Usage: $0 (ldap|preflight|dump|ldifdump|krenew)" >&2
    exit 1
    ;;
esac
